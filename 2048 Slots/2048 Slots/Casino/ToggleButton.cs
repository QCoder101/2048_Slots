using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2048_Slots.Casino
{
    class ToggleButton : Button
    {
        public ToggleButton() : base() { }

        public ToggleButton(int x, int y, int width, int height) : base(x, y, width, height) { }

        public bool CheckTapped(Vector2 TappedPosition)
        {
            if (TappedPosition.X >= PositionRectangle.X && PositionRectangle.X + PositionRectangle.Width >= TappedPosition.X &&
               TappedPosition.Y >= PositionRectangle.Y && PositionRectangle.Y + PositionRectangle.Height >= TappedPosition.Y)
            {
                if (Pressed)
                {
                    Image = OriginalImage;
                    Pressed = false;
                    return true;
                }
                else
                {
                    Image = PressedImage;
                    Pressed = true;
                    return true;
                }
            }
            return false;
        }

        public bool CheckReleased(Vector2 TappedPosition)
        {
            if (TappedPosition.X >= PositionRectangle.X && PositionRectangle.X + PositionRectangle.Width >= TappedPosition.X &&
               TappedPosition.Y >= PositionRectangle.Y && PositionRectangle.Y + PositionRectangle.Height >= TappedPosition.Y)
            {
                return true;
            }

            return false;
        }
    }
}
