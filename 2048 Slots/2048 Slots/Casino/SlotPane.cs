using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2048_Slots.Casino
{
    class SlotPane : Pane
    {
        public static SpriteFont FONT;

        // The Width of the Square 
        public static int SQUARE_WIDTH = 80;

        private int value;

        public bool DontCheck;

        public Color PaneColor;

        public SlotPane() : base() { }

        public SlotPane(int x, int y, int width, int height) : base(x, y, width, height) { }

        public void SetValue(int value)
        {
            this.value = value;
        }

        public void MakeTextureWithoutColor(GraphicsDevice graphicsDevice)
        {
            Image = new Texture2D(graphicsDevice, PositionRectangle.Width, PositionRectangle.Height);

            Color[] data = new Color[PositionRectangle.Width * PositionRectangle.Height];

            for (int i = 0; i < data.Length; ++i) data[i] = PaneColor;
            Image.SetData(data);
        }

        public void MakeTexture(GraphicsDevice graphicsDevice)
        {
            switch (value)
            {
                case 2: PaneColor = new Color(237, 227, 217);
                    break;
                case 4: PaneColor = new Color(235, 223, 197);
                    break;
                case 8: PaneColor = new Color(245, 175, 115);
                    break;
                case 16: PaneColor = new Color(239, 155, 95);
                    break;
                case 32: PaneColor = new Color(239, 129, 92);
                    break;
                case 64: PaneColor = new Color(235, 94, 67);
                    break;
                case 128: PaneColor = new Color(244, 207, 119);
                    break;
                case 256: PaneColor = new Color(236, 206, 96);
                    break;
                case 512: PaneColor = new Color(240, 200, 79);
                    break;
                case 1024: PaneColor = new Color(235, 199, 59);
                    break;
                case 2048: PaneColor = new Color(236, 196, 0);
                    break;
                default: PaneColor = new Color(204, 191, 179);
                    break;
            }

            MakeTextureWithoutColor(graphicsDevice);
        }

        public void Draw(SpriteBatch spriteBatch, GraphicsDevice graphicsDevice)
        {
            base.Draw(spriteBatch);

            Vector2 size = FONT.MeasureString(value.ToString());

            float x = PositionRectangle.X + ((SQUARE_WIDTH) / 2 - (size.X / 2));
            float y = PositionRectangle.Y + ((SQUARE_WIDTH) / 2 - (size.Y / 2));

            Color color = Color.White;

            if (value <= 4) color = new Color(118, 110, 101);

            spriteBatch.Begin();
            spriteBatch.DrawString(FONT, Convert.ToString(value), new Vector2(x, y), color);
            spriteBatch.End();
        }

        public bool Update(int moveSpeed)
        {
            PositionRectangle.Y += moveSpeed;
            if (!DontCheck) return PositionRectangle.Y == Wheels.ROW_Y + Wheels.ROW_HEIGHT;
            return false;
        }

        public int GetValue()
        {
            return value;
        }
    }
}
