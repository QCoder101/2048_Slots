using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2048_Slots.Casino
{
    class TextPane
    {
        public  static SpriteFont   FONT;

        private Vector2             PositionVector;
        public  string              Message;

        public  Color               MessageColor, BoarderColor;

        public  bool                DrawBoarder;

        private Rectangle           BoarderRect;
        private Texture2D           BoarderTexture;

        public TextPane(int x, int y, int width, int height)
        {
            PositionVector  = new Vector2(x, y);
            BoarderRect     = new Rectangle(x, y, width, height);
        }

        public void ChangeMessage(string NewMessage)
        {
            Message = NewMessage;
        }

        public void DrawRectangle(SpriteBatch spriteBatch, int lineWidth)
        {
            if (BoarderTexture == null)
            {
                BoarderTexture = new Texture2D(spriteBatch.GraphicsDevice, 1, 1);
                BoarderTexture.SetData<Color>(new Color[] { Color.White });
            }

            spriteBatch.Begin();
            spriteBatch.Draw(BoarderTexture, new Rectangle(BoarderRect.X, BoarderRect.Y, lineWidth, BoarderRect.Height + lineWidth), BoarderColor);
            spriteBatch.Draw(BoarderTexture, new Rectangle(BoarderRect.X, BoarderRect.Y, BoarderRect.Width + lineWidth, lineWidth), BoarderColor);
            spriteBatch.Draw(BoarderTexture, new Rectangle(BoarderRect.X + BoarderRect.Width, BoarderRect.Y, lineWidth, BoarderRect.Height + lineWidth), BoarderColor);
            spriteBatch.Draw(BoarderTexture, new Rectangle(BoarderRect.X, BoarderRect.Y + BoarderRect.Height, BoarderRect.Width + lineWidth, lineWidth), BoarderColor);
            spriteBatch.End();
        }     

        public void Draw(SpriteBatch spriteBatch)
        {
            if(DrawBoarder) DrawRectangle(spriteBatch, 4);
            DrawText(spriteBatch);
        }

        private void BoarderDraw(SpriteBatch spriteBatch)
        {
            if (BoarderTexture == null) return;
            spriteBatch.Begin();
            spriteBatch.Draw(BoarderTexture, BoarderRect, BoarderColor);
            spriteBatch.End();
        }

        private void DrawText(SpriteBatch spriteBatch)
        {
            CenterPositionText(Message, BoarderRect.X, BoarderRect.Y, BoarderRect.Width, BoarderRect.Height);

            spriteBatch.Begin();
            spriteBatch.DrawString(FONT, Message, PositionVector, MessageColor);
            spriteBatch.End();
        }

        public void CenterPositionText(string Text, int StartingX, int StartingY, int Width, int Height)
        {
            Vector2 size = FONT.MeasureString(Text);

            PositionVector.X = StartingX + ((BoarderRect.Width) / 2 - (size.X / 2));
            PositionVector.Y = StartingY + ((BoarderRect.Height) / 2 - (size.Y / 2));
        }

        public void SetRectangle(Rectangle NewPositionRectangle)
        {
            this.PositionVector.X   = NewPositionRectangle.X;
            this.PositionVector.Y   = NewPositionRectangle.Y;

            this.BoarderRect        = NewPositionRectangle;
        }

        public void MoveUp(int MoveAmount)
        {
            BoarderRect.Y       -= MoveAmount;
            PositionVector.Y    -= MoveAmount;
        }

        public void MoveDown(int MoveAmount)
        {
            BoarderRect.Y       += MoveAmount;
            PositionVector.Y    += MoveAmount;
        }

        public void MoveRight(int MoveAmount)
        {
            BoarderRect.X       += MoveAmount;
            PositionVector.X    += MoveAmount;
        }

        public void MoveLeft(int MoveAmount)
        {
            BoarderRect.X       -= MoveAmount;
            PositionVector.X    -= MoveAmount;
        }
    }
}
