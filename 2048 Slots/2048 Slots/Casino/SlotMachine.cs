using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2048_Slots.Casino
{
    class SlotMachine
    {
        public static int[] WINNINGS = {
                                             4, // Three    2s
                                             6, // Three    4s
                                             8, // Three    8s
                                            10, // Three   16s
                                            12, // Three   32s
                                            16, // Three   64s
                                            20, // Three  128s
                                            25, // Three  256s
                                            50, // Three  512s
                                            75, // Three 1024s
                                           100, // Three 2048s
                                        };

        public static int[] WINNINGS_TWO = {
                                            1, // Two    2s
                                            1, // Two    4s
                                            1, // Two    8s
                                            2, // Two   16s
                                            3, // Two   32s
                                            4, // Two   64s
                                            5, // Two  128s
                                            6, // Two  256s
                                            7, // Two  512s
                                            8, // Two 1024s
                                            9, // Two 2048s
                                        };

        private Rectangle           CHANGE_RECTANGLE        = new Rectangle(30, 460, 115, 60);

        private const  int          ANIMATION_MAX           =  50;
        private const  int          SPIN_BUTTON_DIAMETER    = 100;

        public  static int          STARTING_CREDITS        = 100;
        public  static int          BET_MAX                 =  10;

        private int                 AnimationCount;
        
        public  static int          Credits, Bet, Paid;

        private static bool         ChangeCredits;
        public  static bool         MusicPaused;
        private static int          ChangeAmount;

        private GraphicsDevice      graphicsDevice;
        public  Pane                TopImage, BaseBackground, LeftPane, RightPane;
        public  Button              SpinButton, IncreaseBetArrow, DecreaseBetArrow, ResetButton;
        public  ToggleButton        MusicButton;
        private SlotWheels          wheels;

        private TextPane            CreditsPane, CreditsPaneTitle, BetsPane, BetsPaneTitle, PaidPane, PaidPaneTitle, ChangeAnimation, PaidChangeAnimation;

        public void Reset()
        {
            Credits = STARTING_CREDITS - 1;

            Bet = 1;

            Paid = 0;

            ChangeCredits = false;

            // The spinning Images of the wheels
            wheels = new SlotWheels(graphicsDevice);
        }

        public SlotMachine(GraphicsDevice graphicsDevice)
        {
            this.graphicsDevice = graphicsDevice;

            Reset();

            // Making the Panes
            TopImage            = new Pane(0, 0, 480, 150);
            LeftPane            = new Pane(0, 150, 65, 280);
            RightPane           = new Pane(415, 150, 65, 280);
            BaseBackground      = new Pane(0, Wheels.ROW_Y + Wheels.ROW_HEIGHT, 480, 370);

            // Making the buttons for the user interactions
            SpinButton = new Button(480 / 2 - SPIN_BUTTON_DIAMETER / 2, Wheels.ROW_Y + Wheels.ROW_HEIGHT + (420 / 2 - SPIN_BUTTON_DIAMETER / 2) + 10, SPIN_BUTTON_DIAMETER, SPIN_BUTTON_DIAMETER);

            // Making the pane to display the credits
            CreditsPane = new TextPane(30, 460, 115, 60) { MessageColor = Color.Red, DrawBoarder = true, BoarderColor = Color.DarkGray };

            CreditsPaneTitle = new TextPane(30, 520, 115, 60) { Message = "Credits", MessageColor = Color.White };
            CreditsPaneTitle.CenterPositionText(CreditsPaneTitle.Message, 30, 520, 115, 60);

            // Making the pane to display the bets
            BetsPane = new TextPane(180, 460, 80, 60) { MessageColor = Color.Red, DrawBoarder = true, BoarderColor = Color.DarkGray };

            BetsPaneTitle = new TextPane(180, 520, 80, 60) { Message = "Bets", MessageColor = Color.White };
            BetsPaneTitle.CenterPositionText(BetsPaneTitle.Message, 180, 520, 80, 60);

            // The Buttons to increase or decrease the current bet
            IncreaseBetArrow = new Button(250, 475, 40, 40);
            DecreaseBetArrow = new Button(155, 475, 40, 40);

            // Making the pane to display the amount paid
            PaidPane = new TextPane(300, 460, 150, 60) { MessageColor = Color.Red, DrawBoarder = true, BoarderColor = Color.DarkGray };

            PaidPaneTitle = new TextPane(300, 520, 150, 60) { Message = "Paid", MessageColor = Color.White };
            PaidPaneTitle.CenterPositionText(PaidPaneTitle.Message, 300, 520, 150, 60);

            ChangeAnimation = new TextPane(CHANGE_RECTANGLE.X, CHANGE_RECTANGLE.Y, CHANGE_RECTANGLE.Width, CHANGE_RECTANGLE.Height) { MessageColor = Color.White };

            PaidChangeAnimation = new TextPane(300, CHANGE_RECTANGLE.Y, CHANGE_RECTANGLE.Width, CHANGE_RECTANGLE.Height) { MessageColor = Color.White };

            MusicButton = new ToggleButton(50, Wheels.ROW_Y + Wheels.ROW_HEIGHT + (420 / 2 - SPIN_BUTTON_DIAMETER / 2) + 10, 75, 105);
            ResetButton = new Button(350, Wheels.ROW_Y + Wheels.ROW_HEIGHT + (420 / 2 - SPIN_BUTTON_DIAMETER / 2) + 10, 75, 105);
        }

        public void Update()
        {
            wheels.Update();
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            wheels.Draw(spriteBatch);

            TopImage.Draw(spriteBatch);
            LeftPane.Draw(spriteBatch);
            RightPane.Draw(spriteBatch);

            BaseBackground.Draw(spriteBatch);

            SpinButton.Draw(spriteBatch);

            CreditsPane.ChangeMessage(Credits.ToString());
            CreditsPane.Draw(spriteBatch);

            CreditsPaneTitle.Draw(spriteBatch);

            BetsPane.ChangeMessage(Bet.ToString());
            BetsPane.Draw(spriteBatch);

            BetsPaneTitle.Draw(spriteBatch);

            IncreaseBetArrow.Draw(spriteBatch);
            DecreaseBetArrow.Draw(spriteBatch);

            PaidPane.ChangeMessage(Paid.ToString());
            PaidPane.Draw(spriteBatch);

            PaidPaneTitle.Draw(spriteBatch);

            MusicButton.Draw(spriteBatch);
            ResetButton.Draw(spriteBatch);

            if (ChangeCredits)
            {
                if (ChangeAmount > 0)
                {
                    ChangeAnimation.ChangeMessage("+" + ChangeAmount.ToString());
                    PaidChangeAnimation.ChangeMessage("+" + ChangeAmount.ToString());

                    PaidChangeAnimation.CenterPositionText(ChangeAnimation.Message, 300, CHANGE_RECTANGLE.Y, CHANGE_RECTANGLE.Width, CHANGE_RECTANGLE.Height);
                    PaidChangeAnimation.Draw(spriteBatch);
                    PaidChangeAnimation.MoveUp(1);
                }
                else ChangeAnimation.ChangeMessage(ChangeAmount.ToString());

                ChangeAnimation.CenterPositionText(ChangeAnimation.Message, CHANGE_RECTANGLE.X, CHANGE_RECTANGLE.Y, CHANGE_RECTANGLE.Width, CHANGE_RECTANGLE.Height);
                ChangeAnimation.Draw(spriteBatch);
                ChangeAnimation.MoveUp(1);

                if (AnimationCount >= ANIMATION_MAX)
                {
                    AnimationCount  = 0;
                    ChangeCredits   = false;
                    ChangeAmount    = 0;
                    ChangeAnimation.SetRectangle(CHANGE_RECTANGLE);

                    PaidChangeAnimation.SetRectangle(new Rectangle(300, CHANGE_RECTANGLE.Y, CHANGE_RECTANGLE.Width, CHANGE_RECTANGLE.Height));
                }
                else AnimationCount++;
            }
        }

        public void CheckTapped(Microsoft.Xna.Framework.Vector2 TappedPosition)
        {
            if (SpinButton.CheckTapped(TappedPosition) && Bet > 0)
            {
                if (Bet > 0 && Credits >= 0)
                {
                    //Credits -= Bet;
                    wheels.Activate();
                    if (Credits < 0)
                    {
                        Bet += Credits;
                        Credits = 0;
                    }
                }
            }

            if (IncreaseBetArrow.CheckTapped(TappedPosition) && !SlotWheels.Started)
            {
                if (Bet + 1 <= Credits && Bet + 1 <= BET_MAX)
                {
                    Bet++;
                    Credits--;
                }
            }

            if (DecreaseBetArrow.CheckTapped(TappedPosition) && !SlotWheels.Started)
            {
                if (Bet - 1 >= 0)
                {
                    Bet--;
                    Credits++;
                }
            }

            if (MusicButton.CheckTapped(TappedPosition))
            {
                if (MusicButton.Pressed)
                {
                    MediaPlayer.Pause();
                    MusicPaused = true;
                }
                else
                {
                    MediaPlayer.Resume();
                    MusicPaused = false;
                }
            }

            if (ResetButton.CheckTapped(TappedPosition)) Reset();
        }

        public void CheckReleased(Microsoft.Xna.Framework.Vector2 TappedPosition)
        {
            SpinButton.CheckReleased(TappedPosition);

            IncreaseBetArrow.CheckReleased(TappedPosition);
            DecreaseBetArrow.CheckReleased(TappedPosition);

            MusicButton.CheckReleased(TappedPosition);
            ResetButton.CheckReleased(TappedPosition);
        }

        public static void DecreaseCredits()
        {
            if (Bet >= 0)
            {
                ChangeCredits = true;
                ChangeAmount = -Bet;
                Credits -= Bet;
            }
        }

        public static void IncreaseCredits(int IncreaseAmount)
        {
            if (IncreaseAmount > 0)
            {
                if (IncreaseAmount > 5) ChangeAmount = IncreaseAmount * Bet / 2;
                else ChangeAmount = IncreaseAmount * Bet;

                ChangeCredits = true;
                Credits += ChangeAmount;
                Paid += ChangeAmount;
            }
        }
    }
}
