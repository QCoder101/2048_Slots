using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2048_Slots.Casino
{
    class SlotWheels
    {
        private const int NUM_OF_WHEELS     = 3;
        private const int SPIN_DIFFERENCE   = 13;

        private int[] WHEEL_SPEED = { 30, 30, 30 };

        private Wheels[] wheels;

        public  static bool Started;

        public SlotWheels(GraphicsDevice graphicsDevice)
        {
            wheels = new Wheels[NUM_OF_WHEELS];
            Started = false;

            MakeWheels(graphicsDevice);
        }
            
        private void MakeWheels(GraphicsDevice graphicsDevice)
        {
            int x = 480 / 2 - ((Wheels.ROW_SPACING * 2) + (Wheels.ROW_WIDTH * 3)) / 2;

            for (int i = 0; i < NUM_OF_WHEELS; i++)
            {
                wheels[i] = new Wheels(graphicsDevice) { MoveSpeed = WHEEL_SPEED[i], SpinMax = (i + 1) * SPIN_DIFFERENCE };
                wheels[i].SetRectangle(x + (Wheels.ROW_WIDTH + Wheels.ROW_SPACING) * i, Wheels.ROW_Y, Wheels.ROW_WIDTH, Wheels.ROW_HEIGHT);
                wheels[i].MakeTexture();
            }
        }

        public void Update()
        {
            wheels[0].Update();
            wheels[1].Update();
            wheels[2].Update();

            if (Started && !wheels[2].Activated)
            {
                MediaPlayer.Stop();

                MediaPlayer.Play(Game1.BackgroundMusic);
                MediaPlayer.IsRepeating = true;
                if (SlotMachine.MusicPaused) MediaPlayer.Pause();

                Started = false;
                CheckForWinnings();
            }
        }

        public void Activate()
        {
            if (!Started)
            {
                MediaPlayer.Play(Game1.SpinningMusic);
                MediaPlayer.IsRepeating = true;

                if (SlotMachine.MusicPaused) MediaPlayer.Pause();

                Started = true;
                wheels[0].Start();
                wheels[1].Start();
                wheels[2].Start();

                SlotMachine.DecreaseCredits();
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < NUM_OF_WHEELS; i++) wheels[i].Draw(spriteBatch);
        }


        public void CheckForWinnings()
        {
            int IncreaseValue = 0;
            IncreaseValue += CheckVertical();
            IncreaseValue += CheckHorizontal();
            IncreaseValue += CheckDiagonal();    

            SlotMachine.IncreaseCredits(IncreaseValue);
        }

        private int CheckDiagonal()
        {
            int[][] InBoundValues = new int[3][];
            int CurrentValue = 0;
            bool Diagonal = false;

            for (int i = 0; i < wheels.Length; i++) InBoundValues[i] = wheels[i].GetInBoundValues();

            Diagonal = InBoundValues[0][0] == InBoundValues[1][1] && InBoundValues[1][1] == InBoundValues[2][2];

            if (Diagonal)
            {
                CurrentValue += SlotMachine.WINNINGS[GetPower(InBoundValues[1][1], 2) - 1];
                wheels[0].ChangePaneColor(0, Color.LightGreen);
                wheels[1].ChangePaneColor(1, Color.LightGreen);
                wheels[2].ChangePaneColor(2, Color.LightGreen);
            }

            Diagonal = InBoundValues[0][2] == InBoundValues[1][1] && InBoundValues[1][1] == InBoundValues[2][0];

            if (Diagonal)
            {
                CurrentValue += SlotMachine.WINNINGS[GetPower(InBoundValues[1][1], 2) - 1];
                wheels[0].ChangePaneColor(2, Color.LightGreen);
                wheels[1].ChangePaneColor(1, Color.LightGreen);
                wheels[2].ChangePaneColor(0, Color.LightGreen);
            }

            return CurrentValue;
        }

        private int CheckHorizontal()
        {
            int[][] InBoundValues = new int[3][];
            int Value = 0;

            for (int i = 0; i < wheels.Length; i++)
            {
                for(int j = 0; j < wheels.Length; j++) InBoundValues[j] = wheels[j].GetInBoundValues();

                int[] RowValues = { InBoundValues[0][i], InBoundValues[1][i], InBoundValues[2][i] };
                
                if (RowValues[0] == RowValues[1] && RowValues[1] == RowValues[2])
                {
                    Value += SlotMachine.WINNINGS[GetPower(RowValues[1], 2) - 1];

                    for (int j = 0; j < wheels.Length; j++) wheels[j].ChangePaneColor(i, Color.LightGreen);
                }
                //else if (RowValues[0] == RowValues[1] || RowValues[1] == RowValues[2])
                //{
                //    Value += SlotMachine.WINNINGS_TWO[GetPower(RowValues[1], 2) - 1];

                //    if (RowValues[0] == RowValues[1]) wheels[0].ChangePaneColor(i, Color.LightGreen);
                //    else if (RowValues[2] == RowValues[1]) wheels[2].ChangePaneColor(i, Color.LightGreen);

                //    wheels[1].ChangePaneColor(i, Color.LightGreen);
                //}
            }

            return Value;
        }

        private int CheckVertical()
        {
            int[] InBoundValues = null;
            int Value = 0;

            for (int i = 0; i < wheels.Length; i++)
            {
                InBoundValues = wheels[i].GetInBoundValues();

                if (InBoundValues[0] == InBoundValues[1] && InBoundValues[1] == InBoundValues[2])
                {
                    Value += SlotMachine.WINNINGS[GetPower(InBoundValues[0], 2) - 1];

                    for (int j = 0; j < wheels.Length; j++)
                    {
                        wheels[i].ChangePaneColor(j, Color.LightGreen);
                    }
                }
                //else if (InBoundValues[0] == InBoundValues[1] || InBoundValues[1] == InBoundValues[2])
                //{
                //    Value += SlotMachine.WINNINGS_TWO[GetPower(InBoundValues[1], 2) - 1];

                //    if (InBoundValues[0] == InBoundValues[1]) wheels[i].ChangePaneColor(0, Color.LightGreen);
                //    else if (InBoundValues[2] == InBoundValues[1]) wheels[i].ChangePaneColor(2, Color.LightGreen);

                //    wheels[i].ChangePaneColor(1, Color.LightGreen);
                //}
            }

            return Value;
        }

        private int GetPower(int Value, int Base)
        {
            int number = 0;

            while (Math.Pow(Base, number) != Value && Math.Pow(Base, number) < Math.Abs(Value))
            {
                number++;
            }

            return number;
        }
    }
}
