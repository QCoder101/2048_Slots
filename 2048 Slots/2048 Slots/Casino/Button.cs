using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2048_Slots.Casino
{
    class Button : Pane
    {
        public Texture2D PressedImage, OriginalImage;
        public bool      Pressed;

        public Button() : base() { Pressed = false; }

        public Button(int x, int y, int width, int height) : base(x, y, width, height) { Pressed = false; }

        public bool CheckTapped(Vector2 TappedPosition)
        {
            if (TappedPosition.X >= PositionRectangle.X && PositionRectangle.X + PositionRectangle.Width >= TappedPosition.X &&
               TappedPosition.Y >= PositionRectangle.Y && PositionRectangle.Y + PositionRectangle.Height >= TappedPosition.Y)
            {
                Image = PressedImage;
                Pressed = true;
                return true;
            }

            return false;
        }

        public bool CheckReleased(Vector2 TappedPosition)
        {
            if (TappedPosition.X >= PositionRectangle.X && PositionRectangle.X + PositionRectangle.Width >= TappedPosition.X &&
               TappedPosition.Y >= PositionRectangle.Y && PositionRectangle.Y + PositionRectangle.Height >= TappedPosition.Y)
            {
                Image = OriginalImage;
                Pressed = false;
                return true;
            }

            return false;
        }
    }
}
