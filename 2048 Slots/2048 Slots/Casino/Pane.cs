using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2048_Slots.Casino
{
    class Pane
    {
        public Texture2D Image;
        public Rectangle PositionRectangle;

        public Pane() : this(0, 0, 0, 0) { }

        public Pane(int x, int y, int width, int height)
        {
            SetRectangle(x, y, width, height);
        }

        public void SetRectangle(int x, int y, int width, int height)
        {
            this.PositionRectangle = new Rectangle(x, y, width, height);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(Image, PositionRectangle, Color.White);
            spriteBatch.End();
        }
    }
}
