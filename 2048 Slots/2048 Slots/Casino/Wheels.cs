﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2048_Slots.Casino
{
    class Wheels
    {
        public  static SoundEffect      SpinStopSound;

        public  static Texture2D        SHADOW;

        public  static int              ROW_SPACING     =  10;
        public  static int              ROW_WIDTH       = 100;
        public  static int              ROW_HEIGHT      = 280;
        public  static int              ROW_Y           = 150;
        
        private const int               START_MAX       =   6;

        private GraphicsDevice          graphicsDevice;

        private Rectangle               positionRectangle;

        private Texture2D               RowTexture;
        private Color                   RowColor;

        private LinkedList<SlotPane>    RowPanes;


        public  bool                    Activated;
        private bool                    Spawn, Starting;

        private static Random           Rand;

        public  int                     MoveSpeed, SpinMax; 
        private int                     StartCount, SpinCount, Speed;

        public Wheels(GraphicsDevice graphicsDevice)
        {
            this.graphicsDevice     = graphicsDevice;
            this.RowColor           = Color.White;
            if (Rand == null) Rand  = new Random();
            this.Activated          = false;
        }

        public void MakeTexture()
        {
            RowTexture = new Texture2D(graphicsDevice, ROW_WIDTH, ROW_HEIGHT);
            Color[] data = new Color[ROW_WIDTH * ROW_HEIGHT];

            for (int i = 0; i < data.Length; ++i) data[i] = RowColor;
            RowTexture.SetData(data);

            MakeStartingPanes();
        }

        public void SetRectangle(int x, int y, int width, int height)
        {
            positionRectangle = new Rectangle(x, y, width, height);
        }

        private void MakeStartingPanes()
        {
            int y = ROW_Y - SlotPane.SQUARE_WIDTH;

            RowPanes = new LinkedList<SlotPane>();

            SlotPane newPane = new SlotPane(positionRectangle.X + (100 / 2 - SlotPane.SQUARE_WIDTH / 2), y, SlotPane.SQUARE_WIDTH, SlotPane.SQUARE_WIDTH);
            newPane.SetValue((int)Math.Pow(2, Rand.Next(11) + 1));
            newPane.MakeTexture(graphicsDevice);
            RowPanes.AddLast(newPane);

            y += SlotPane.SQUARE_WIDTH + 10;
            newPane = new SlotPane(positionRectangle.X + (100 / 2 - SlotPane.SQUARE_WIDTH / 2), y, SlotPane.SQUARE_WIDTH, SlotPane.SQUARE_WIDTH);
            newPane.SetValue((int)Math.Pow(2, Rand.Next(11) + 1));
            newPane.MakeTexture(graphicsDevice);
            RowPanes.AddLast(newPane);

            y += SlotPane.SQUARE_WIDTH + 10;
            newPane = new SlotPane(positionRectangle.X + (100 / 2 - SlotPane.SQUARE_WIDTH / 2), y, SlotPane.SQUARE_WIDTH, SlotPane.SQUARE_WIDTH);
            newPane.SetValue((int)Math.Pow(2, Rand.Next(11) + 1));
            newPane.MakeTexture(graphicsDevice);
            RowPanes.AddLast(newPane);

            y += SlotPane.SQUARE_WIDTH + 10;
            newPane = new SlotPane(positionRectangle.X + (100 / 2 - SlotPane.SQUARE_WIDTH / 2), y, SlotPane.SQUARE_WIDTH, SlotPane.SQUARE_WIDTH);
            newPane.SetValue((int)Math.Pow(2, Rand.Next(11) + 1));
            newPane.MakeTexture(graphicsDevice);
            RowPanes.AddLast(newPane);

            y += SlotPane.SQUARE_WIDTH + 10;
            newPane = new SlotPane(positionRectangle.X + (100 / 2 - SlotPane.SQUARE_WIDTH / 2), y, SlotPane.SQUARE_WIDTH, SlotPane.SQUARE_WIDTH);
            newPane.SetValue((int)Math.Pow(2, Rand.Next(11) + 1));
            newPane.MakeTexture(graphicsDevice);
            newPane.DontCheck = true;
            RowPanes.AddLast(newPane);

            y += SlotPane.SQUARE_WIDTH + 10;
            newPane = new SlotPane(positionRectangle.X + (100 / 2 - SlotPane.SQUARE_WIDTH / 2), y, SlotPane.SQUARE_WIDTH, SlotPane.SQUARE_WIDTH);
            newPane.SetValue((int)Math.Pow(2, Rand.Next(11) + 1));
            newPane.MakeTexture(graphicsDevice);
            newPane.DontCheck = true;
            RowPanes.AddLast(newPane);
        }

        public void Update()
        {
            if (Activated)
            {
                if (!Starting) Speed = MoveSpeed;
                else 
                {
                    Speed = -10;
                    StartCount++;
                    if (StartCount >= START_MAX)
                    {
                        StartCount = 0;
                        Starting   = false;
                    }
                }

                for (int i = 0; i < RowPanes.Count; i++)
                {
                    if (RowPanes.ElementAt(i).Update(Speed))
                    {
                        RowPanes.RemoveLast();
                        Spawn = true;
                    }
                }

                if (Spawn)
                {
                    Spawn = false;
                    SlotPane newPane = new SlotPane(positionRectangle.X + 10, ROW_Y - SlotPane.SQUARE_WIDTH,
                                                    SlotPane.SQUARE_WIDTH, SlotPane.SQUARE_WIDTH);
                    //newPane.SetValue((int)Math.Pow(2, Rand.Next(11) + 1));

                    int newValue = GetNewValue();

                    if (newValue == RowPanes.ElementAt(0).GetValue() && Rand.Next(10) >= 5) newValue = GetNewValue();

                    newPane.SetValue(newValue);
                    newPane.MakeTexture(graphicsDevice);
                    RowPanes.AddFirst(newPane);

                    SpinCount++;
                    if (SpinCount >= SpinMax)
                    {
                        SpinCount = 0;
                        Activated = false;
                    }
                }
            }
        }

        private int GetNewValue()
        {
            int randNumber = Rand.Next(100) + 1;
            int power = 0;

            if      (randNumber <=  15) power =  1;
            else if (randNumber <=  30) power =  2;
            else if (randNumber <=  45) power =  3;
            else if (randNumber <=  60) power =  4;
            else if (randNumber <=  70) power =  5;
            else if (randNumber <=  78) power =  6;
            else if (randNumber <=  84) power =  7;
            else if (randNumber <=  88) power =  8;
            else if (randNumber <=  92) power =  9;
            else if (randNumber <=  96) power = 10;
            else if (randNumber <= 100) power = 11;

            return (int)Math.Pow(2, power);
        }

        public void Start()
        {
            Activated = Starting = true;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(RowTexture, positionRectangle, Color.White);
            spriteBatch.End();

            for(int i = 0; i < RowPanes.Count; i++)
                RowPanes.ElementAt(i).Draw(spriteBatch, graphicsDevice);

            spriteBatch.Begin();
            spriteBatch.Draw(SHADOW, positionRectangle, Color.White * 0.75f);
            spriteBatch.End();
        }

        public int[] GetInBoundValues()
        {
            int[] Values = new int[3];
            int y = 0;

            for (int i = 0; i < RowPanes.Count; i++)
            {
                y = RowPanes.ElementAt(i).PositionRectangle.Y;
                if (y >= Wheels.ROW_Y && y <= Wheels.ROW_Y + Wheels.ROW_HEIGHT)
                {
                    for(int j = 0; j < Values.Length; j++)
                    {
                        if (Values[j] == 0)
                        {
                            Values[j] = RowPanes.ElementAt(i).GetValue();
                            break;
                        }
                    }
                }
            }

            return Values;
        }

        public void ChangePaneColor(int PaneLocation, Color newColor)
        {
            int y = 0;
            int currentPaneLocation = 0;

            for (int i = 0; i < RowPanes.Count; i++)
            {
                y = RowPanes.ElementAt(i).PositionRectangle.Y;
                if (y >= Wheels.ROW_Y && y <= Wheels.ROW_Y + Wheels.ROW_HEIGHT)
                {
                    if (currentPaneLocation == PaneLocation) 
                    {
                        RowPanes.ElementAt(i).PaneColor = newColor;
                        RowPanes.ElementAt(i).MakeTextureWithoutColor(graphicsDevice);
                        break;
                    }

                    currentPaneLocation++;
                }
            }
        }
    }
}
