using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;
using _2048_Slots.Casino;
using Microsoft.Advertising.Mobile.Xna;
using System.Diagnostics;

namespace _2048_Slots
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public static Song BackgroundMusic, SpinningMusic;

        //Making a first ad unit
        private DrawableAd drawableAd;

        private const string APP_ID = "a4e8ca04-ca3b-4158-ba93-c9b53fc0278d";
        private const string AD_UNIT_ID = "178013";

        SlotMachine machine;

        private const long INCREASE_SPEED = 200;
        private long IncreaseTime;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.SupportedOrientations = DisplayOrientation.Portrait;
            graphics.PreferredBackBufferWidth = 480;
            graphics.PreferredBackBufferHeight = 800;
            Content.RootDirectory = "Content";

            // Frame rate is 30 fps by default for Windows Phone.
            TargetElapsedTime = TimeSpan.FromTicks(333333);

            // Extend battery life under lock.
            InactiveSleepTime = TimeSpan.FromSeconds(1);
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            SlotPane.FONT                           = Content.Load<SpriteFont>("Fonts\\TileNumberFont");
            TextPane.FONT                           = Content.Load<SpriteFont>("Fonts\\CreditsFont");

            Wheels.SHADOW                           = Content.Load<Texture2D>("Images\\RowShadow");

            machine                                 = new SlotMachine(graphics.GraphicsDevice);

            machine.TopImage.Image                  = Content.Load<Texture2D>("Images\\2048 Slots Header");
            machine.LeftPane.Image                  = Content.Load<Texture2D>("Images\\LeftPane");
            machine.RightPane.Image                 = Content.Load<Texture2D>("Images\\RightPane");
            machine.BaseBackground.Image            = Content.Load<Texture2D>("Images\\2048 Slots Base");

            machine.SpinButton.OriginalImage        = Content.Load<Texture2D>("Images\\Spin Button");
            machine.SpinButton.Image                = Content.Load<Texture2D>("Images\\Spin Button");
            machine.SpinButton.PressedImage = Content.Load<Texture2D>("Images\\Spin Button Pressed");

            machine.IncreaseBetArrow.OriginalImage  = Content.Load<Texture2D>("Images\\Increase_Button");
            machine.IncreaseBetArrow.Image          = Content.Load<Texture2D>("Images\\Increase_Button");
            machine.IncreaseBetArrow.PressedImage   = Content.Load<Texture2D>("Images\\Increase_Button_Pressed");

            machine.DecreaseBetArrow.OriginalImage  = Content.Load<Texture2D>("Images\\Decrease_Button");
            machine.DecreaseBetArrow.Image          = Content.Load<Texture2D>("Images\\Decrease_Button");
            machine.DecreaseBetArrow.PressedImage   = Content.Load<Texture2D>("Images\\Decrease_Button_Pressed");

            machine.MusicButton.OriginalImage       = Content.Load<Texture2D>("Images\\SoundOnButton");
            machine.MusicButton.Image               = Content.Load<Texture2D>("Images\\SoundOnButton");
            machine.MusicButton.PressedImage        = Content.Load<Texture2D>("Images\\SoundOffButton");
            
            machine.ResetButton.OriginalImage       = Content.Load<Texture2D>("Images\\ResetButton");
            machine.ResetButton.Image               = Content.Load<Texture2D>("Images\\ResetButton");
            machine.ResetButton.PressedImage        = Content.Load<Texture2D>("Images\\ResetButtonPressed");

            BackgroundMusic                         = Content.Load<Song>("Songs\\BackgroundMusic");
            MediaPlayer.Play(Game1.BackgroundMusic);
            MediaPlayer.IsRepeating = true;

            SpinningMusic                           = Content.Load<Song>("Songs\\SpinningMusic");

            //Wheels.SpinStopSound                    = Content.Load<SoundEffect>("SoundEffects\\SpinStopSound");

            IncreaseTime                            = INCREASE_SPEED;

            //Initializing AdUnit
            AdGameComponent.Initialize(this, APP_ID);
            Components.Add(AdGameComponent.Current);
            //Setting the ad unit position and dimensions in the form a rectangle
            Microsoft.Xna.Framework.Rectangle rect2 = new Microsoft.Xna.Framework.Rectangle(0, 800 - 80, 480, 80);
            //Setting the code for the first ad unit
            drawableAd = AdGameComponent.Current.CreateAd(AD_UNIT_ID, rect2, true);
            //Sets the refresh of the ad unit
            drawableAd.AdRefreshed += new EventHandler(drawableAd_AdRefreshed);
            //Handles errors by the ad unit
            drawableAd.ErrorOccurred += new EventHandler<Microsoft.Advertising.AdErrorEventArgs>(drawableAd_ErrorOccurred);
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here
            machine.Update();

            if (machine.IncreaseBetArrow.Pressed && IncreaseTime <= 0)
            {
                if (SlotMachine.Bet + 1 <= SlotMachine.Credits && SlotMachine.Bet + 1 <= SlotMachine.BET_MAX && !SlotWheels.Started)
                {
                    SlotMachine.Bet++;
                    SlotMachine.Credits--;
                }
                IncreaseTime = INCREASE_SPEED;
            }
            else if (machine.DecreaseBetArrow.Pressed && IncreaseTime <= 0)
            {
                if (SlotMachine.Bet - 1 >= 0 && !SlotWheels.Started)
                {
                    SlotMachine.Bet--;
                    SlotMachine.Credits++;
                }
                IncreaseTime = INCREASE_SPEED;
            }
            else IncreaseTime -= (long)gameTime.ElapsedGameTime.TotalMilliseconds;

            TouchCollection touchCollection = TouchPanel.GetState();
            foreach (TouchLocation tl in touchCollection)
            {
                if (tl.State == TouchLocationState.Pressed)
                {
                    machine.CheckTapped(tl.Position);
                    IncreaseTime = INCREASE_SPEED;
                }
                if (tl.State == TouchLocationState.Released) machine.CheckReleased(tl.Position);
            }

            // update the ad control
            AdGameComponent.Current.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            machine.Draw(spriteBatch);

            //Drawing the ad control
            AdGameComponent.Current.Draw(gameTime);

            base.Draw(gameTime);
        }

        private void drawableAd_ErrorOccurred(object sender, Microsoft.Advertising.AdErrorEventArgs e)
        {
            //If there is an error in the ad units write "Ad error occurred!"
            Debug.WriteLine("Ad error occurred!");
        }

        private void drawableAd_AdRefreshed(object sender, EventArgs e)
        {
            //When ad refreshes writes "Ad refreshed!"
            Debug.WriteLine("Ad refreshed!");
        }
    }
}
